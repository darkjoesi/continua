'use strict';
module.exports = (sequelize, DataTypes) => {
    const curso_activo = sequelize.define('curso_activo', {
        external_carrera: DataTypes.UUID,
        external_id: DataTypes.UUID,
        precio: {type: DataTypes.DOUBLE, defaultValue: 0.0},
        certificado: {type: DataTypes.BOOLEAN, defaultValue: false},
        horas: {type: DataTypes.INTEGER, defaultValue: 0},
        
        fecha_inicio: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        fecha_final: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        estado: DataTypes.ENUM(['INSCRIPCION', 'DESARROLLO','EJECUCION', 'FINALIZADO', 'CANCELADO']),
        tipo: DataTypes.ENUM(['PRESENCIAL', 'SEMIPRESENCIAL', 'VIRTUAL']),
        tipo_cert: DataTypes.ENUM(['NINGUNO','PARTICIPACION', 'APROBACION']),
        poster: {type: DataTypes.STRING(150), defaultValue:"no_poster.png"},
        horario: {type: DataTypes.STRING(100), defaultValue:"NO_DATA"},
        docente: {type: DataTypes.STRING(100), defaultValue:"NO_DATA"},
        silabo: {type: DataTypes.STRING(100), defaultValue:"NO_DATA"},
        resumen: {type: DataTypes.STRING(250), defaultValue:"NO_DATA"},
        descripcion: {type: DataTypes.STRING(2000), defaultValue:"NO_DATA"}

    }, {freezeTableName: true});
    curso_activo.associate = function (models) {
        // associations can be defined here
        //curso_activo.hasMany(models.persona, {foreignKey: 'id_rol', as: 'persona'});
        curso_activo.belongsTo(models.curso, {foreignKey: 'id_curso'});
        curso_activo.hasMany(models.inscripcion, {foreignKey: 'id_curso_activo', as: 'inscripcion'});
    };
    return curso_activo;
};