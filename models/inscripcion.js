'use strict';
module.exports = (sequelize, DataTypes) => {
    const inscripcion = sequelize.define('inscripcion', {
        external_carrera: DataTypes.UUID,        
        
        fecha: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        
        estado: {type: DataTypes.BOOLEAN, defaultValue:true}
        

    }, {freezeTableName: true});
    inscripcion.associate = function (models) {
        // associations can be defined here
        //inscripcion.hasMany(models.persona, {foreignKey: 'id_rol', as: 'persona'});
        inscripcion.belongsTo(models.curso_activo, {foreignKey: 'id_curso_activo'});
        inscripcion.belongsTo(models.persona, {foreignKey: 'id_persona'});
    };
    return inscripcion;
};