'use strict';
module.exports = (sequelize, DataTypes) => {
  const persona = sequelize.define('persona', {
    cedula: {type: DataTypes.STRING(10), unique: true, allowNull: false},
    apellidos: DataTypes.STRING(30),
    nombres: DataTypes.STRING(30),
    celular: DataTypes.STRING(15),    
    correo: DataTypes.STRING(100),
    external_id: DataTypes.UUID,
    external_carrera: DataTypes.UUID,
    curso: {type: DataTypes.STRING(100), defaultValue:"NO_DATA"},
    paralelo:   DataTypes.ENUM(['A', 'B', 'C', 'D', 'E']),
    tipo: DataTypes.ENUM(['PARTICULAR', 'ESTUDIANTE', 'EGRESADO', 'GRADUADO'])    
  }, {freezeTableName: true});
  persona.associate = function(models) {
    // associations can be defined here
    persona.hasOne(models.cuenta, {foreignKey: 'id_persona', as: 'cuenta'});
    persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
    persona.hasMany(models.inscripcion, {foreignKey: 'id_persona', as: 'persona'});
  };
  return persona;
};