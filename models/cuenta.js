'use strict';
module.exports = (sequelize, DataTypes) => {
  const cuenta = sequelize.define('cuenta', {
    correo: {type: DataTypes.STRING(50), unique: true, allowNull: false},
    clave: DataTypes.STRING(70),
    external_id: DataTypes.UUID,
    estado: {type: DataTypes.BOOLEAN, defaultValue:true}
  }, {freezeTableName: true});
  cuenta.associate = function(models) {
    // associations can be defined here
    cuenta.belongsTo(models.persona, {foreignKey: 'id_persona'});
  };
  return cuenta;
};