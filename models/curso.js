'use strict';
module.exports = (sequelize, DataTypes) => {
  const curso = sequelize.define('curso', {
    nombre: DataTypes.STRING(200),
    estado: {type: DataTypes.BOOLEAN, defaultValue:true},
    poster: {type: DataTypes.STRING(150), defaultValue:"no_poster.png"},
    video: {type: DataTypes.STRING(150), defaultValue:"no_video"},
    external_carrera: DataTypes.UUID,
    external_id: DataTypes.UUID
  }, {freezeTableName: true});
  curso.associate = function(models) {
    // associations can be defined here
    curso.hasMany(models.curso_activo, {foreignKey: 'id_curso', as: 'curso_activo'});
  };
  return curso;
};