var express = require('express');
var router = express.Router();
var api = require('../controls/ApiRest');
var apiRest = new api();

router.get('/cursos', apiRest.getCursos);

/*router.get('/', function (req, res, next) {
    var models = require('./../models');
    models.sequelize.sync().then(() => {
        console.log('Se ha conectado la bd');
        res.send('Se ha sincronizado la bd');
    }).catch(err => {
        console.log(err, "Hubo un error");
        res.send('No se pudo sincronizar bd');
    });
});*/



module.exports = router;
