var express = require('express');
var router = express.Router();

var servicio = require('../controls/service/Servicio');
var service = new servicio();

var auth = function middleWare(req, res, next) {
    //console.log(req.path);
    var segmento = req.path.split("/");//explode("/", req.path);
      //  console.log(segmento + " " + req.user.rol);
        if (req.isAuthenticated() && (req.user.rol == "administrador" || req.user.rol == "coordinador")) {         
                next();
        } else if(req.isAuthenticated() && (req.user.rol != "administrador" && req.user.rol != "coordinador")) {
            req.flash('error', 'No esta autorizado a ver esto');                
                res.redirect('/');
        } else {
            req.flash('error', 'Se requiere iniciar sesion');
            res.redirect('/inicioSesion');
        }
    }



/* GET home page. */
router.get('/',  auth, function(req, res, next) {
    service.getPrincipal(req, res).visualizar();
  //res.render('administrador/template', { title: 'Express' });
  //res.render('administrador/template', { title: 'Educación continua', fragmento: "../administrador/home" });
});


router.get('/principal', auth, function(req, res, next) {
  res.render('administrador/template', { title: 'Educación continua' });
});

//CURSO
router.get('/curso',  auth, function(req, res, next) {   
  service.getCursoControl(req, res).visualizar();
});
router.post('/curso/guardar', auth, function(req, res, next) {   
  service.getCursoControl(req, res).guardar();
});
router.post('/curso/editar', auth, function(req, res, next) {   
  service.getCursoControl(req, res).modificar();
});
router.post('/curso/editar/foto', auth, function(req, res, next) {   
  service.getCursoControl(req, res).modificarFoto();
});
router.get('/curso/estado/:external', auth, function(req, res, next) {   
  service.getCursoControl(req, res).cambiarEstado();
});

//FIN CURSO

//CURSO ACTIVO
router.get('/curso-activo',  auth, function(req, res, next) {   
  service.getCursoActivoControl(req, res).visualizar();
});
router.get('/curso-activo/registro', auth, function(req, res, next) {   
  service.getCursoActivoControl(req, res).visualizar_registro();
});
router.get('/curso-activo/modificar/:external', auth, function(req, res, next) {   
  service.getCursoActivoControl(req, res).visualizar_modficar();
});
router.post('/curso-activo/guardar',  auth,function(req, res, next) {   
  service.getCursoActivoControl(req, res).guardar();
});
router.post('/curso-activo/editar', auth, function(req, res, next) {   
  service.getCursoActivoControl(req, res).modificar();
});
router.post('/curso-activo/editar/foto', auth, function(req, res, next) {   
  service.getCursoActivoControl(req, res).modificarFoto();
});
router.get('/curso-activo/estado/:external/:estado',  auth, function(req, res, next) {   
  service.getCursoActivoControl(req, res).cambiarEstado();
});
//FIN CURSO ACTIVO

//USUARIOS
router.get('/usuarios', function(req, res, next) {   
  service.getUsuariosControl(req, res).visualizar();
});

router.get('/usuario/rol/:external/:estado',  auth,function(req, res, next) {   
  service.getUsuariosControl(req, res).cambiarRol();
});

router.get('/usuario/estado/:external',  auth, function(req, res, next) {   
  service.getUsuariosControl(req, res).cambiarEstado();
});

//INCRITOS
router.get('/inscritos',   function(req, res, next) {   
  service.getCursoActivoControl(req, res).lista_inscritos();
});
//INSCRITOS

//AJAX
router.get('/inscritos/filtrado/:external',   function(req, res, next) {   
  service.getCursoActivoControl(req, res).lista_cursos_ajax();
});
router.get('/private/privado',  auth, function (req, res, next) {
    var models = require('./../models');
    models.sequelize.sync().then(() => {
        console.log('Se ha conectado la bd');
        res.send('Se ha sincronizado la bd');
    }).catch(err => {
        console.log(err, "Hubo un error");
        res.send('No se pudo sincronizar bd');
    });
});



module.exports = router;
