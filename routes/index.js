var express = require('express');
var router = express.Router();
var passport = require('passport');
var servicio = require('../controls/service/ServicioU');
var service = new servicio();

router.get('/', function(req, res, next) {
  //res.render('usuario/template', { fragmento:"../usuario/usuario/principal", title: 'Educacion continua' });
  service.getPrincipalControl(req, res).visualizar();
});
router.get('/informacionCurso/:external', function(req, res, next) {
  //res.render('usuario/template', { fragmento:"../usuario/usuario/principal", title: 'Educacion continua' });
  service.getPrincipalControl(req, res).visualizarCurso();
});

router.get('/inicioSesion', function(req, res, next) {  
  service.getRegistroControl(req, res).visualizarLogin();
});

router.get('/cerrarSesion', function(req, res, next) {  
  service.getRegistroControl(req, res).cerrar();
});

router.get('/registro', function(req, res, next) {  
  service.getRegistroControl(req, res).visualizarRegistro();
});

router.post('/registro/guardar', function(req, res, next) {  
  service.getRegistroControl(req, res).registrar();
});

router.post('/inicio_sesion',
        passport.authenticate('local-signin',
        {
            successRedirect: '/',
            failureRedirect: '/inicioSesion',
            failureFlash: true
        }
    )
);

router.get('/inscribete/:external_curso', function(req, res, next) {  
  service.getPrincipalControl(req, res).postular();
});

module.exports = router;
