'use strict';
var models = require('../models/');
var uuid = require('uuid');
var rol = models.rol;

var persona = models.persona;
var utiles = require('./utilidades/Utilidades');
var req = "";
var res = "";

class RegistroController {
    constructor(req1, res1) {
        req = req1;
        res = res1;
        
    }
    visualizarLogin() {
        res.render('login/login', {title: 'Educacion continua', msg: {error: req.flash('error'), info: req.flash('info')}});
    }

    visualizarRegistro() {
        res.render('login/registro', {title: 'Educacion continua', msg: {error: req.flash('error'), info: req.flash('info')}});
    }

    registrar() {

        if (req.body.correo && req.body.correo != "" &&
                req.body.apellidos && req.body.apellidos != "" &&
                req.body.nombres && req.body.nombres != "" &&
                req.body.correo && req.body.correo != "" &&
                req.body.clave && req.body.clave != "") {
            rol.findOne({where: {nombre: "particular"}}).then(function (role) {
                if (role) {
                    var data = {
                        correo: req.body.correo,
                        apellidos: req.body.apellidos.toUpperCase(),
                        nombres: req.body.nombres.toUpperCase(),
                        cedula: req.body.cedula,
                        celular: "NO_DATA",
                        id_rol: role.id,
                        tipo: "PARTICULAR",
                        paralelo: "A",
                        carrera: "NO_DATA",
                        external_id: uuid.v4(),
                        cuenta: {
                            correo: req.body.correo,
                            clave: req.body.clave,
                            external_id: uuid.v4()
                        }
                    };

                    persona.create(data, {include: [{model: models.cuenta, as: "cuenta"}]}).then(function (result) {
                        if (result) {
                            req.flash('info', utiles.mensajes("1"));
                            res.redirect("/inicioSesion");
                        } else {
                            req.flash('error', utiles.mensajes("3"));
                            res.redirect("/registro");
                        }
                    }).catch(function (err) {
                        // print the error details
                        console.log(err);
                        req.flash('error', utiles.mensajes("3"));
                        res.redirect("/registro");
                    });

                } else {
                    req.flash('error', utiles.mensajes("3"));
                    res.redirect("/registro");
                }
            });

        } else {
            req.flash('error', utiles.mensajes("4"));
            res.redirect("/registro");
        }
    }

    
    /*visualizarCurso() {
     var external = req.params.external;
     curso_activo.findOne({where: {external_id: external}, include: [{model: models.curso}]}).then(function (curs) {
     if (curs) {
     res.render('usuario/template', {fragmento: "../usuario/usuario/curso", title: 'Educacion continua', curso: curs, msg: {error: req.flash('error'), info: req.flash('info')}});
     } else {
     req.flash('error', "No se encuentra el curso");
     res.redirect("/");
     //res.render('usuario/template', {fragmento: "../usuario/usuario/principal", title: 'Educacion continua', curso:curs,msg: {error: req.flash('error'), info: req.flash('info')}});
     }
     });
     }*/
    cerrar() {
        req.session.destroy();
        res.redirect("/");
    }
    
    
    
}
module.exports = RegistroController;