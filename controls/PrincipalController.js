'use strict';
var models = require('../models/');
var curso = models.curso;
var persona = models.persona;
var curso_activo = models.curso_activo;
var inscripcion = models.inscripcion;
var utiles = require('./utilidades/Utilidades');
var req = "";
var res = "";
var user = "";
class PrincipalController {
    constructor(req1, res1) {
        req = req1;
        res = res1;

        if (typeof req.session.passport != 'undefined') {
            user = req.user;
            console.log("exo---------------");
            console.log(req.user);
            console.log(user.rol + "---------------");
        } else {
            console.log("nada");
            user = "";
        }
    }
    visualizar() {
//curso_activo.findAll({include: [{model: models.tipoSitio}, {model: models.galeria, as: "galeria"}]}).then(function (lista) {
//console.log("&&&&&&&&&");
// console.log(req);
//console.log("***********");
//where: {estado: true}
//curso_activo.findAll({where: {estado: 'INSCRIPCION'}, include: [{model: models.curso}]});
        curso_activo.findAll({include: [{model: models.curso}]}).then(function (lista) {
            var cont = 0;
            var cont1 = 0;
            var cont2 = 0;
            var nuevos = [];
            var pasados = [];
            var desarrollo = [];
            lista.forEach(g => {
               // console.log(g.estado);
                if (g.estado == "INSCRIPCION") {
                    nuevos[cont] = g;
                    cont++;
                } else if(!(g.estado == "DESARROLLO")) {
                    pasados[cont1] = g;
                    cont1++;
                } else {
                    desarrollo[cont2] = g;
                    cont2++;
                }

            });
         //   console.log(user.rol + " *******888");
            res.render('usuario/template', {fragmento: "../usuario/usuario/principal", title: 'Educacion continua', nuevos: nuevos, pasados: pasados, desarrollo: desarrollo, user: user});
            //res.render('administrador/template', {title: 'Cursos activos', fragmento: "../administrador/curso_activo/lista", nuevos:nuevos, pasados: pasados, msg: {error: req.flash('error'), info: req.flash('info')}});
        });
    }

    visualizarCurso() {
        var external = req.params.external;
        curso_activo.findOne({where: {external_id: external}, include: [{model: models.curso}]}).then(function (curs) {
            if (curs) {
                if (user != '') {
                    persona.findOne({where: {external_id:user.id}}).then(function (personita) {
                        if (personita) {
                            console.log("*************");
                            console.log(personita);
                            console.log("*************");
                            inscripcion.findOne({where: {id_curso_activo:curs.id, id_persona:personita.id}}).then(function (inscrito) {
                                var ya = false;
                                if (inscrito) {
                                    ya = true;
                                }
                                res.render('usuario/template', {fragmento: "../usuario/usuario/curso", title: 'Educacion continua', curso: curs, msg: {error: req.flash('error'), info: req.flash('info')}, user: user, ya: ya});
                            });
                        } else {
                            res.render('usuario/template', {fragmento: "../usuario/usuario/curso", title: 'Educacion continua', curso: curs, msg: {error: req.flash('error'), info: req.flash('info')}, user: user, ya: false});
                        }
                    });
                } else {
                    res.render('usuario/template', {fragmento: "../usuario/usuario/curso", title: 'Educacion continua', curso: curs, msg: {error: req.flash('error'), info: req.flash('info')}, user: user, ya: false});
                }

            } else {
                //req.flash('error', "No se encuentra el curso");
                res.redirect("/");
                //res.render('usuario/template', {fragmento: "../usuario/usuario/principal", title: 'Educacion continua', curso:curs,msg: {error: req.flash('error'), info: req.flash('info')}});
            }
        });
    }

    postular() {
        var external_user = user.id;
        var external_curso = req.params.external_curso;
        if (external_user && external_user != '' && external_curso && external_curso != '') {
            //if (external_curso && external_curso != '') {
            curso_activo.findOne({where: {external_id: external_curso}, include: [{model: models.curso}]}).then(function (curs) {
                if (curs) {
                    persona.findOne({where: {external_id: external_user}}).then(function (persona) {
                        if (persona) {
                            var data = {
                                id_curso_activo: curs.id,
                                id_persona: persona.id,
                                external_carrera: "NO_DATA"
                            };
                            inscripcion.create(data).then(function (result) {
                                if (result) {
                                    req.flash('info', utiles.mensajes(1));
                                    res.redirect("/informacionCurso/" + external_curso);
                                } else {
                                    req.flash('error', utiles.mensajes(0));
                                    res.redirect("/informacionCurso/" + external_curso);
                                }
                            });
                        } else {
                            req.flash('error', utiles.mensajes(0));
                            res.redirect("/informacionCurso/" + external_curso);
                        }
                    });

                    //res.render('usuario/template', {fragmento: "../usuario/usuario/curso", title: 'Educacion continua', curso:curs,msg: {error: req.flash('error'), info: req.flash('info')}, user:user});
                } else {
                    req.flash('error', "No se encuentra el curso");
                    res.redirect("/");
                    //res.render('usuario/template', {fragmento: "../usuario/usuario/principal", title: 'Educacion continua', curso:curs,msg: {error: req.flash('error'), info: req.flash('info')}});
                }
            });
        } else {
            req.flash('error', utiles.mensajes("3"));
            if (external_curso) {
                res.redirect("/informacionCurso/" + external_curso);
            } else {
                res.redirect("/");
            }

        }
    }

}
module.exports = PrincipalController;