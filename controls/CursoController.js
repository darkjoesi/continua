/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
var models = require('../models/');
var curso = models.curso;
var utiles = require('./utilidades/Utilidades');

var uuid = require('uuid');
var formidable = require('formidable');
var extensiones = ["jpg", "png", "gif", "jpeg"];
var maxSize = 1 * 1024 * 1024;
var fs = require('fs');

var req = "";
var res = "";
class CursoController {

    constructor(req1, res1) {
        req = req1;
        res = res1;
    }
    /**
     * Funcion que permite mostrar el listado de sitios
     * @param {type} req Objeto request
     * @param {type} res Objeto response
     * @returns {undefined} Respuesta
     */
    visualizar() {
        //curso.findAll({include: [{model: models.tipoSitio}, {model: models.galeria, as: "galeria"}]}).then(function (lista) {
        //console.log("&&&&&&&&&");
        // console.log(req);
        //console.log("***********");
        curso.findAll().then(function (lista) {
            res.render('administrador/template', {title: 'Sitios turisticos', fragmento: "../administrador/curso/lista", lista: lista, msg: {error: req.flash('error'), info: req.flash('info')}});
        });
    }

    guardar() {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            //    console.log(files);
            //   console.log(fields);
            if (files.archivo.size <= maxSize) {
                var extension = files.archivo.name.split(".").pop().toLowerCase();
                if (extensiones.includes(extension)) {
                    var external = uuid.v4();
                    var nombreFoto = external + "." + "png";
                    fs.rename(files.archivo.path, "public/images/uploads/" + nombreFoto,
                            function (err) {
                                if (err) {
                                    req.flash('error', "Hubo un error " + err);
                                    res.redirect("/administrador/cursos");
                                } else {
                                    var data = {
                                        nombre: fields.nombre.toUpperCase(),
                                        estado: true,
                                        external_id: external,
                                        poster: nombreFoto
                                    };
                                    //var creado = await curso.create(data);
                                    curso.create(data).then(function (result) {
                                        if (result) {
                                            req.flash('info', utiles.mensajes(1));
                                            res.redirect("/administrador/curso");
                                        } else {
                                            req.flash('error', utiles.mensajes(0));
                                            res.redirect("/administrador/curso");
                                        }

                                    });
                                }
                            });
                } else {
                    req.flash('error', "El tipo de archivo no es valido, debe ser png, jpg, o gif");
                    res.redirect("/administrador/curso");
                }
            } else {
                req.flash('error', "El tamano no puede superar 1 MB");
                res.redirect("/administrador/curso");
            }
        });

    }

    modificar() {
        curso.findOne({where: {external_id: req.body.external}}).then(function (cat) {
            cat.nombre = req.body.nombre.toUpperCase();
            cat.save().then(function (result) {
                req.flash('info', utiles.mensajes("2"));
                res.redirect("/administrador/curso");
            });

        });
    }

    modificarFoto() {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            //    console.log(files);
            //   console.log(fields);
            if (files.archivo.size <= maxSize) {
                var extension = files.archivo.name.split(".").pop().toLowerCase();
                if (extensiones.includes(extension)) {
                    var external = fields.external;
                    console.log("*********** " + external);
                    var nombreFoto = external + "." + "png";
                    curso.findOne({where: {external_id: external}}).then(function (cat) {
                        fs.rename(files.archivo.path, "public/images/uploads/" + nombreFoto,
                                function (err) {
                                    if (err) {
                                        req.flash('error', "Hubo un error " + err);
                                        res.redirect("/administrador/cursos");
                                    } else {
                                        cat.poster = nombreFoto;
                                        cat.save().then(function (result) {
                                            req.flash('info', utiles.mensajes("2"));
                                            res.redirect("/administrador/curso");
                                        });
                                    }
                                });
                    });

                } else {
                    req.flash('error', "El tipo de archivo no es valido, debe ser png, jpg, o gif");
                    res.redirect("/administrador/curso");
                }
            } else {
                req.flash('error', "El tamano no puede superar 1 MB");
                res.redirect("/administrador/curso");
            }
        });

    }

    cambiarEstado() {
        var external = req.params.external;
        curso.findOne({where: {external_id: external}}).then(function (cat) {
            if (cat) {
                if(cat.estado) {
                    cat.estado = false;
                    req.flash('info', utiles.mensajes("5"));
                } else {
                    cat.estado = true;
                    req.flash('info', utiles.mensajes("6"));
                }
                
                cat.save().then(function (result) {
                    
                    res.redirect("/administrador/curso");
                });
            } else {
                req.flash('info', utiles.mensajes("3"));
                res.redirect("/administrador/curso");
            }


        });
    }

}
module.exports = CursoController;