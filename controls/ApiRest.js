'use strict';
var models = require('../models/');
var uuid = require('uuid');
var curso = models.curso;
class ApiRest {
    //************* CURSO  ***************
    getCursos(req, res) {
        curso.findAll({
            attributes: ['nombre', 'estado', 'external_id', 'poster', 'video'],
            where: {estado: true}
            //,
            //include: [{model: models.sitio, as: 'sitio', include: [{model: models.galeria, as: 'galeria'}]}]
        }).then(function (lista) {

            if (lista) {
                res.json({msg: "Listado de cursos", code: "200", cursos: lista});
            } else {
                res.json({msg: "No hay cursos", code: "200", cursos: lista});
            }
            //res.render('index', {title: 'Sitios turisticos', fragmento: 'fragmento/categoria/lista', lista: lista, 
            //    msg: {error: req.flash('error'),info: req.flash('info')}});
        });
    }
    registrarCurso(req, res) {
        if (req.body.nombre && req.body.nombre != "") {

            var data = {
                nombre: req.body.nombre,                
                external_id: uuid.v4()
                
            };

            //curso.create(data, {include: [{model: models.cuenta, as: "cuenta"}]}).then(function (newP) {
            curso.create(data).then(function (newP) {
                if (newP) {
                    res.json({msg: "Se ha registrado correctamente", code: "200", "nombre":newP.nombre})
                } else {
                    res.json({msg: "No se pudo crear su cuenta", code: "404"})
                }
            });




        } else {
            res.json({msg: "Faltan datos al registro", code: "404"})
        }
    }
    //************* FIN CURSO ************
}
module.exports = ApiRest;