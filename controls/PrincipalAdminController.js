/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
var models = require('../models/');
var curso = models.curso;
var curso_activo = models.curso_activo;
var inscripcion = models.inscripcion;


var req = "";
var res = "";
class PrincipalAdminController {

    constructor(req1, res1) {
        req = req1;
        res = res1;
    }
    /**
     * Funcion que permite mostrar el listado de sitios
     * @param {type} req Objeto request
     * @param {type} res Objeto response
     * @returns {undefined} Respuesta
     */
    visualizar() {
        curso_activo.findAll({
            where:{estado:"INSCRIPCION"},
            include: [{model: models.curso}, {model:models.inscripcion, as: 'inscripcion'}]
        }).then(function (cursos) {
            //console.log(cursos);
            inscripcion.findAll({
                include: [ {model: models.curso_activo,  include: [{model: models.curso}]},{model: models.persona, attributes: ['apellidos', 'nombres']}],
                order: [
                    ['id', 'DESC']
                ],
            }).then(function (lista) {
               // console.log("///////////////////////-----------");
                
               // console.log(lista);
               // console.log("///////////////////////-----------");
                //res.render('administrador/template', { title: 'Educación continua', fragmento: "../administrador/home" });
                res.render('administrador/template', {title: 'Inscritos', fragmento: "../administrador/home",
                    lista: lista, estado: curso_activo.rawAttributes.estado.values, cursos: cursos,
                    msg: {error: req.flash('error'), info: req.flash('info')}});
            });
        });
    }

    modificarCuenta() {
        
    }

}
module.exports = PrincipalAdminController;