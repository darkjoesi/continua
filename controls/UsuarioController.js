/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
var models = require('../models/');
var persona = models.persona;
var rol = models.rol;
var cuenta = models.cuenta;
var utiles = require('./utilidades/Utilidades');

/*var uuid = require('uuid');
 var formidable = require('formidable');
 var extensiones = ["jpg", "png", "gif", "jpeg"];
 var maxSize = 1 * 1024 * 1024;
 var fs = require('fs');
 */
var req = "";
var res = "";
class UsuarioController {

    constructor(req1, res1) {
        req = req1;
        res = res1;
    }
    /**
     * Funcion que permite mostrar el listado de sitios
     * @param {type} req Objeto request
     * @param {type} res Objeto response
     * @returns {undefined} Respuesta
     */
    visualizar() {
        //curso.findAll({include: [{model: models.tipoSitio}, {model: models.galeria, as: "galeria"}]}).then(function (lista) {
        //console.log("&&&&&&&&&");
        // console.log(req);
        //console.log("***********");
        rol.findAll().then(function (roles) {
            persona.findAll({include: [{model: models.rol}, {model: models.cuenta, as: "cuenta"}]}).then(function (lista) {
                res.render('administrador/template', {title: 'Educacion continua', fragmento: "../administrador/usuarios/lista", lista: lista,
                    roles: roles,
                    msg: {error: req.flash('error'), info: req.flash('info')}});
            });
        });

    }

    cambiarRol() {
        var external = req.params.external;
        var estado = req.params.estado;
        rol.findAll().then(function (roles) {
            var esta = false;
            roles.forEach(function (item1, index) {
                if (item1.nombre == estado) {
                    esta = true;
                }
            });
            if (esta) {

                rol.findOne({where: {nombre: estado}}).then(function (rolsito) {
                    persona.findOne({where: {external_id: external}}).then(function (cat) {
                        console.log(cat);
                        if (cat) {
                            cat.id_rol = rolsito.id;

                            cat.save().then(function (result) {
                                req.flash('info', utiles.mensajes("2"));
                                res.redirect("/administrador/usuarios");
                            });
                        } else {
                            req.flash('error', utiles.mensajes("3"));
                            res.redirect("/administrador/usuarios");
                        }


                    }).catch(function (err) {
                        // print the error details
                        console.log(err);
                        req.flash('error', utiles.mensajes("3"));
                        res.redirect("/administrador/usuarios");
                    });
                });
            } else {

                req.flash('error', utiles.mensajes("4"));
                res.redirect("/administrador/usuarios");
            }
        });


    }

    cambiarEstado() {
        var external = req.params.external;
        persona.findOne({where: {external_id: external}}).then(function (cat) {
            if (cat) {

                cuenta.findOne({where: {id_persona: cat.id}}).then(function (cuentita) {
                    if (cuentita.estado) {
                        cuentita.estado = false;
                        req.flash('info', utiles.mensajes("5"));
                    } else {
                        cuentita.estado = true;
                        req.flash('info', utiles.mensajes("6"));
                    }

                    cuentita.save().then(function (result) {

                        res.redirect("/administrador/usuarios");
                    });
                });


            } else {
                req.flash('info', utiles.mensajes("3"));
                res.redirect("/administrador/usuarios");
            }


        });
    }

}
module.exports = UsuarioController;