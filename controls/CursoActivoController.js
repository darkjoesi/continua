/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
var models = require('../models/');
var curso = models.curso;
var curso_activo = models.curso_activo;
var inscripcion = models.inscripcion;
var utiles = require('./utilidades/Utilidades');
var uuid = require('uuid');
var formidable = require('formidable');
var extensiones = ["jpg", "png", "gif", "jpeg"];
var maxSize = 1 * 1024 * 1024;
var fs = require('fs');
var req = "";
var res = "";
class CursoActivoController {

    constructor(req1, res1) {
        req = req1;
        res = res1;
    }
    /**
     * Funcion que permite mostrar el listado de sitios
     * @param {type} req Objeto request
     * @param {type} res Objeto response
     * @returns {undefined} Respuesta
     */
    visualizar() {
        //curso_activo.findAll({include: [{model: models.tipoSitio}, {model: models.galeria, as: "galeria"}]}).then(function (lista) {
        //console.log("&&&&&&&&&");
        // console.log(req);
        //console.log("***********");
        curso_activo.findAll({include: [{model: models.curso}]}).then(function (lista) {
            res.render('administrador/template', {title: 'Cursos activos',
                fragmento: "../administrador/curso_activo/lista",
                estado: curso_activo.rawAttributes.estado.values,
                lista: lista, msg: {error: req.flash('error'), info: req.flash('info')}});
        });
    }

    visualizar_registro() {


        curso.findAll({where: {estado: true}}).then(function (lista) {
            res.render('administrador/template', {title: 'Cursos activos', fragmento: "../administrador/curso_activo/registro",
                lista: lista, estado: curso_activo.rawAttributes.estado.values, tipo: curso_activo.rawAttributes.tipo.values,
                tipo_cert: curso_activo.rawAttributes.tipo_cert.values,
                msg: {error: req.flash('error'), info: req.flash('info')}});
        });
    }

    guardar() {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            //    console.log(files);
            //   console.log(fields);
            if (files.archivo.size <= maxSize) {
                var extension = files.archivo.name.split(".").pop().toLowerCase();
                if (extensiones.includes(extension)) {
                    var external = uuid.v4();
                    var nombreFoto = external + "." + "png";
                    fs.rename(files.archivo.path, "public/images/uploads/curso-activo/" + nombreFoto,
                            function (err) {
                                if (err) {
                                    req.flash('error', "Hubo un error " + err);
                                    res.redirect("/administrador/curso_activos");
                                } else {
                                    console.log(fields.certificado + " *** ");
                                    var isCert = false;
                                    if (fields.certificado && fields.certificado == 'on') {
                                        isCert = true;
                                    }
                                    curso.findOne({where: {external_id: fields.curso}}).then(function (cat) {
                                        if (cat) {
                                            var data = {
                                                external_carrera: "NO",
                                                precio: fields.precio,
                                                certificado: isCert,
                                                horas: fields.horas,
                                                resumen: fields.resumen,
                                                fecha_inicio: fields.fecha_inicio,
                                                fecha_final: fields.fecha_final,
                                                estado: fields.estado,
                                                tipo: fields.tipo,
                                                horario: fields.horario,
                                                docente: fields.docente.toUpperCase(),
                                                descripcion: fields.descripcion,
                                                external_id: external,
                                                poster: nombreFoto,
                                                tipo_cert: fields.tipo_cert,
                                                silabo: "NO",
                                                id_curso: cat.id
                                            };
                                            //var creado = await curso_activo.create(data);
                                            //insert into cuenta values ();
                                            curso_activo.create(data).then(function (result) {
                                                if (result) {
                                                    req.flash('info', utiles.mensajes(1));
                                                    res.redirect("/administrador/curso-activo");
                                                } else {
                                                    req.flash('error', utiles.mensajes(0));
                                                    res.redirect("/administrador/curso-activo");
                                                }

                                            });
                                        } else {
                                            req.flash('error', utiles.mensajes(0));
                                            res.redirect("/administrador/curso-activo");
                                        }

                                    });
                                }
                            });
                } else {
                    req.flash('error', "El tipo de archivo no es valido, debe ser png, jpg, o gif");
                    res.redirect("/administrador/curso_activo");
                }
            } else {
                req.flash('error', "El tamano no puede superar 1 MB");
                res.redirect("/administrador/curso_activo");
            }
        });
    }

    visualizar_modficar() {
        var external_id = req.params.external;
        if (external_id) {
            curso.findAll({where: {estado: true}}).then(function (lista) {
                curso_activo.findOne({where: {external_id: external_id}, include: [{model: models.curso}]}).then(function (activo) {
                    if (activo) {
                        res.render('administrador/template', {title: 'Cursos activos', fragmento: "../administrador/curso_activo/modificar",
                            lista: lista, estado: curso_activo.rawAttributes.estado.values, tipo: curso_activo.rawAttributes.tipo.values,
                            tipo_cert: curso_activo.rawAttributes.tipo_cert.values, activo: activo,
                            msg: {error: req.flash('error'), info: req.flash('info')}});
                    } else {
                        req.flash('error', utiles.mensajes("3"));
                        res.redirect("/administrador/curso-activo");
                    }
                });
            });
        } else {
            req.flash('error', utiles.mensajes("3"));
            res.redirect("/administrador/curso-activo");
        }
    }

    modificar() {
        curso.findOne({where: {external_id: req.body.curso}}).then(function (cursoC) {
            if (cursoC) {
                curso_activo.findOne({where: {external_id: req.body.external}}).then(function (cat) {
                    if (cat) {
                        //cat.nombre = req.body.nombre.toUpperCase();
                        //external_carrera: "NO",
                        var isCert = false;
                        if (req.body.certificado && req.body.certificado == 'on') {
                            isCert = true;
                        }
                        cat.precio = req.body.precio;
                        cat.certificado = isCert;
                        cat.horas = req.body.horas;
                        cat.fecha_inicio = req.body.fecha_inicio;
                        cat.fecha_final = req.body.fecha_final;
                        cat.resumen = req.body.resumen;
                        cat.estado = req.body.estado;
                        cat.tipo = req.body.tipo;
                        cat.horario = req.body.horario;
                        cat.docente = req.body.docente.toUpperCase();
                        cat.descripcion = req.body.descripcion;
                        //external_id: external,
                        //poster: nombreFoto,
                        cat.tipo_cert = req.body.tipo_cert;
                        //silabo: "NO",
                        cat.id_curso = cursoC.id;


                        cat.save().then(function (result) {
                            req.flash('info', utiles.mensajes("2"));
                            res.redirect("/administrador/curso-activo");
                        });
                    } else {
                        req.flash('info', utiles.mensajes("3"));
                        res.redirect("/administrador/curso-activo");
                    }

                });
            } else {
                req.flash('info', utiles.mensajes("3"));
                res.redirect("/administrador/curso-activo");
            }
        });
    }

    modificarFoto() {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            //    console.log(files);
            //   console.log(fields);
            if (files.archivo.size <= maxSize) {
                var extension = files.archivo.name.split(".").pop().toLowerCase();
                if (extensiones.includes(extension)) {
                    var external = fields.external;
                    var nombreFoto = external + "." + "png";
                    curso_activo.findOne({where: {external_id: external}}).then(function (cat) {
                        fs.rename(files.archivo.path, "public/images/uploads/curso-activo/" + nombreFoto,
                                function (err) {
                                    if (err) {
                                        req.flash('error', "Hubo un error " + err);
                                        res.redirect("/administrador/curso-activo");
                                    } else {
                                        cat.poster = nombreFoto;
                                        cat.save().then(function (result) {
                                            req.flash('info', utiles.mensajes("2"));
                                            res.redirect("/administrador/curso-activo");
                                        });
                                    }
                                });
                    });
                } else {
                    req.flash('error', "El tipo de archivo no es valido, debe ser png, jpg, o gif");
                    res.redirect("/administrador/curso-activo");
                }
            } else {
                req.flash('error', "El tamano no puede superar 1 MB");
                res.redirect("/administrador/curso-activo");
            }
        });
    }

    cambiarEstado() {
        var external = req.params.external;
        var estado = req.params.estado;
        var lista = curso_activo.rawAttributes.estado.values;
        if (lista.includes(estado)) {

            curso_activo.findOne({where: {external_id: external}}).then(function (cat) {
                console.log(cat);
                if (cat) {
                    cat.estado = estado;
                    
                    cat.save().then(function (result) {
                        req.flash('info', utiles.mensajes("2"));
                        res.redirect("/administrador/curso-activo");
                    });
                } else {
                    req.flash('error', utiles.mensajes("3"));
                    res.redirect("/administrador/curso-activo");
                }


            }).catch(function (err) {
                // print the error details
                console.log(err);
                req.flash('error', utiles.mensajes("3"));
                res.redirect("/administrador/curso-activo");
            });
        } else {

            req.flash('error', utiles.mensajes("4"));
            res.redirect("/administrador/curso-activo");
        }

    }
    
    lista_inscritos() {
       curso_activo.findAll({
            where:{estado:"INSCRIPCION"},
            include: [{model: models.curso}, {model:models.inscripcion, as: 'inscripcion'}]
        }).then(function (cursos) {
            //console.log(cursos);
            inscripcion.findAll({
                include: [ {model: models.curso_activo,  include: [{model: models.curso}]},{model: models.persona, attributes: ['apellidos', 'nombres','cedula','correo']}],
                order: [
                    ['id', 'DESC']
                ],
            }).then(function (lista) {
                console.log("///////////////////////-----------");
                
                console.log(lista);
                console.log("///////////////////////-----------");
                res.render('administrador/template', {title: 'Inscritos', fragmento: "../administrador/postulaciones/lista",
                    lista: lista, estado: curso_activo.rawAttributes.estado.values, cursos: cursos,
                    msg: {error: req.flash('error'), info: req.flash('info')}});
            });
        });
        
    }
    
    lista_cursos_ajax() {
        var externnal = req.params.external;
        if(externnal == 'NADA') {
            inscripcion.findAll({                
                include: [{model: models.persona}, {model: models.curso_activo, include: [{model: models.curso}]}],
                order: [
                    ['id', 'DESC']
                ],
            }).then(function (lista) {
                console.log(lista);
                res.json({ lista: lista });
                /*res.render('administrador/template', {title: 'Inscritos', fragmento: "../administrador/postulaciones/lista",
                    lista: lista, estado: curso_activo.rawAttributes.estado.values, cursos: cursos,
                    msg: {error: req.flash('error'), info: req.flash('info')}});*/
            });
        } else {
            inscripcion.findAll({                
                include: [{model: models.persona}, {model: models.curso_activo,where:{external_id: externnal, estado:"INSCRIPCION"}, include: [{model: models.curso}]}],
                order: [
                    ['id', 'DESC']
                ],
            }).then(function (lista) {
                console.log(lista);
                res.json({ lista: lista });
                /*res.render('administrador/template', {title: 'Inscritos', fragmento: "../administrador/postulaciones/lista",
                    lista: lista, estado: curso_activo.rawAttributes.estado.values, cursos: cursos,
                    msg: {error: req.flash('error'), info: req.flash('info')}});*/
            });
        }
        
    }   


}
module.exports = CursoActivoController;