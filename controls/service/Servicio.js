/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
var curso = require('../CursoController');
var curso_activo = require('../CursoActivoController');
var usuarios = require('../UsuarioController');
var principal = require('../PrincipalAdminController');

class Service {
    getCursoControl(req, res) {
        var Curso = new curso(req, res);
        return Curso;
    }
    getCursoActivoControl(req, res) {
        var CursoActivo = new curso_activo(req, res);
        return CursoActivo;
    }
    getUsuariosControl(req, res) {
        var Usuarios = new usuarios(req, res);
        return Usuarios;
    }
    getPrincipal(req, res) {
        var Principal = new principal(req, res);
        return Principal;
    }
}
module.exports = Service;


